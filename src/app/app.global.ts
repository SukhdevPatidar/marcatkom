 export const App: any = {
   id: 'ionstore2',
   ContactEmail    : 'rajesh@dlbinfotech.com', // this email wiil be used for email in Account Menu -> Contact (sending email)
   url: 'http://marcatkom.com/',
   OneSignalAppID: 'dcd30f2b-2952-4341-9001-daf62f3c26d8',
   GCM: '896704981923', // Google Project Number for Google Cloud Messaging
   consumerKey: 'ck_9ead017d24298a19343b2f066b46663032816215',
   consumerSecret: 'cs_ce97b42f894668555e75d31bf65152fdf58484d4',

   // PAYPAL
   paypalSandboxClientID: 'AZjyISbp1zmOhZ0o_iAG3W2IGjlz2hvEC-8cGoQ7fXcMFN9afaRuW0X1B1PVSgkSuTQWOKqM9N4NTkOP',
   paypalLiveClientID: '', // get this from paypal developer dashboard
   paypalState: 'PayPalEnvironmentSandbox', // change this to 'PayPalEnvironmentProduction' if you wanna live

   // RazorPay
   RazorColor: '#FF1654', // this will be your coloring style in RazorPay when checkout
   logo: 'https://i.imgur.com/skWtV3H.png', // change this with your own logo
   title: 'Ionstore 2 - Tabs', // this will be your app name shown in RazorPay when checkout

   languages: [
     {id: 'en', title: 'English'},
     {id: 'ar', title: 'Arabic'}
     // {id: 'fr', title: 'French'},
     // {id: 'in', title: 'Hindi'},
     // {id: 'cn', title: 'Chinese'}
   ],
   currencies: [],

   defaultLang: 'en'
 };

// export const App: any = {
//   id: 'ionstore2',
//   ContactEmail    : 'rajesh@dlbinfotech.com', // this email wiil be used for email in Account Menu -> Contact (sending email)
//   url: 'http://childevelop.com/',
//   OneSignalAppID: '7eb350c1-0bcf-4286-8768-6aa9de0d1c25',
//   GCM: '510529775551', // Google Project Number for Google Cloud Messaging
//   consumerKey: 'ck_b69fb48e63e80f07abb51bd2675e1e8884a1123a',
//   consumerSecret: 'cs_e70ee978af9d515c70c2fee733308ae6c0697265',
//
//   // PAYPAL
//   paypalSandboxClientID: 'AZjyISbp1zmOhZ0o_iAG3W2IGjlz2hvEC-8cGoQ7fXcMFN9afaRuW0X1B1PVSgkSuTQWOKqM9N4NTkOP',
//   paypalLiveClientID: '', // get this from paypal developer dashboard
//   paypalState: 'PayPalEnvironmentSandbox', // change this to 'PayPalEnvironmentProduction' if you wanna live
//
//   // RazorPay
//   RazorColor: '#FF1654', // this will be your coloring style in RazorPay when checkout
//   logo: 'https://i.imgur.com/skWtV3H.png', // change this with your own logo
//   title: 'Ionstore 2 - Tabs', // this will be your app name shown in RazorPay when checkout
//
//   languages: [
//     {id: 'en', title: 'English'},
//     {id: 'ar', title: 'Arabic'}
//   ],
//
//   defaultLang: 'en'
// };
