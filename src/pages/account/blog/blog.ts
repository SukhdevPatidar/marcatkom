import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { BlogProvider, ToastProvider, LoadingProvider } from '../../../providers/providers';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-blog',
  templateUrl: 'blog.html',
})
export class BlogPage {
  blogs: any[] = [];
  page: number;
  more: boolean;

  constructor(private blog: BlogProvider, private nav: NavController, private toast: ToastProvider, private translate: TranslateService, private loader: LoadingProvider) {
    this.page = 1;
    this.loader.present();
    this.blog.get().map(blog => blog.json()).subscribe( blog=> {
      this.blogs = blog.posts;
      if (blog.pages > 1)
        this.more = true;
      this.loader.dismiss();
    })
  }

  ionViewWillEnter(){
    
  }

  loadMore(event){
    this.page++;

    this.blog.get(this.page).map(blog => blog.json()).subscribe(blog=>{
      let temp = blog.posts;
      event.complete();
      this.blogs = this.blogs.concat(temp);

      if (this.blogs.length == blog.count_total){
        this.more = false;
        event.enable(false);

        this.translate.get(['NOMORE']).subscribe( x=> {
          this.toast.show(x.NOMORE);
        });
      }
    }, (err) => {
      console.log(err)
    });
  }

  goTo(page, params){
    this.nav.push(page, {params: params});
  }
}
