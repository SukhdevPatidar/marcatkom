import { SharedModule } from '../../../../app/shared.module';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetailBlogPage } from './detail-blog';

@NgModule({
  declarations: [
    DetailBlogPage,
  ],
  imports: [
    IonicPageModule.forChild(DetailBlogPage),
    SharedModule
  ],
})
export class DetailBlogPageModule {}
