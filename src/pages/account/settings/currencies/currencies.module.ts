import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccountSettingsCurrenciesPage } from './currencies';
import { SharedModule } from '../../../../app/shared.module';

@NgModule({
  declarations: [
    AccountSettingsCurrenciesPage,
  ],
  imports: [
    IonicPageModule.forChild(AccountSettingsCurrenciesPage),
    SharedModule
  ],
})
export class AccountSettingsCurrenciesPageModule {}
