import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { App } from '../../../../app/app.global';
import { SettingsProvider, ToastProvider } from '../../../../providers/providers';

@IonicPage()
@Component({
  selector: 'page-account-settings-currencies',
  templateUrl: 'currencies.html',
})
export class AccountSettingsCurrenciesPage {
  private form: FormGroup;

  currencies: any[] = App.currencies;
  tmp: any;

  constructor(public navCtrl: NavController, private settings: SettingsProvider, private toast: ToastProvider, private fb: FormBuilder, public navParams: NavParams, private translate: TranslateService) {
    let currency = localStorage.getItem("MULTI_CURRENCY");
    this.form = this.fb.group({
      curr: [(currency ? currency : this.settings.all.settings ? this.settings.all.settings.currency : this.settings.all.currency), Validators.required]
    });

    this.settings.load().then((x) => {
      this.tmp = x.settings;
    });

  }

  doSubmit() {
    this.tmp.currency = this.form.value.curr;
    localStorage.setItem("MULTI_CURRENCY", this.form.value.curr);
    this.settings.setSettings(this.tmp, 'settings');
    // if (!this.settings.all.settings) {
    //   this.settings.all.settings = {
    //     currency: this.form.value.curr
    //   };
    // } else {
    //   this.settings.all.settings.currency = this.form.value.curr;
    // }

    this.translate.get('CURRENCY_SAVED').subscribe(x => {
      this.toast.show(x);
    });
  }

}
