import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicPage, App, Platform, AlertController, NavController, Events, ModalController, NavParams } from 'ionic-angular';
import { AddressProvider, SettingsProvider, ToastProvider, UserProvider, LoadingProvider, CartProvider, WooCommerceProvider, OrderProvider, HesabepaymentProvider } from '../../providers/providers';
import { TranslateService } from '@ngx-translate/core';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { totalmem } from 'os';

@IonicPage()
@Component({
  selector: 'page-checkout',
  templateUrl: 'checkout.html',
})

export class CheckoutPage {
  checkout: string = "shipping";
  cart: any;
  settings: any;
  billing: any;
  shipping: any;
  zones: any;
  shipping_lines: any[] = [];
  shipping_method: any = [];
  payments: any[] = [];
  coupons: any[] = [];
  tax: any;
  order: any = {};
  total: number = 0;
  allowFreeShipping: boolean = false;

  priceMoney: any = 0;

  stripe: any = {
    no: '',
    month: '',
    year: ''
  };

  razor: any = {};

  coupon: any = {};

  temmZone: any = {};

  months: any = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  years: any = [];

  img: any = {
    stripe: [
      'assets/img/logo/visa.svg',
      'assets/img/logo/mastercard.svg',
      'assets/img/logo/amex.svg',
      'assets/img/logo/diners.svg',
      'assets/img/logo/discover.svg',
      'assets/img/logo/jcb.svg'
    ],
    paypal: [
      'assets/img/logo/visa.svg',
      'assets/img/logo/mastercard.svg',
      'assets/img/logo/amex.svg',
      'assets/img/logo/discover.svg'
    ],
    razorpay: [
      'assets/img/logo/logo-razor.png'
    ]
  };

  language: any;

  constructor(private iab: InAppBrowser, private setting: SettingsProvider, private app: App, private statusBar: StatusBar, private alert: AlertController, private platform: Platform, private nav: NavController, private translate: TranslateService, private toast: ToastProvider, private user: UserProvider, private loader: LoadingProvider, private woo: WooCommerceProvider, private _cart: CartProvider, private events: Events, private _order: OrderProvider, private hesabe: HesabepaymentProvider, private address: AddressProvider, public navParams: NavParams, public modal: ModalController) {
    this.woo.loadPayments().then(x => {
      console.log(x);
      this.payments = x;
    });

    this.woo.loadCoupons().then(x => {
      console.log("cupon");
      console.log(x);
      this.coupons = x;
    }, err => {
      console.log(err);
    });

    this.cart = this._cart;



    if (this.setting.all.zones.length <= 0) {
      this.loader.present();
      this.woo.loadZones();
      this.setting.load().then(x => {
        this.zones = x.zones;
        this.loader.dismiss();
      });

      this.temmZone = JSON.parse(localStorage.getItem('zone'));
      this.zones = this.temmZone;

    }
    else {
      this.zones = this.setting.all.zones;
    }

    // this.woo.getTaxes().then( x=> {
    //   this.tax = x;
    //   console.log(x);
    // })

    this.listenOrder();

    console.log("checkOut");
    console.log(this.payments);

    this.calculateitem(this.cart.all);
  }

  formatPrice = (product) => {
    let multiCurrPrice = product['multi-currency-prices'];
		let currency = this.setting.all.settings?this.setting.all.settings.currency:this.setting.all.currency;
    let priceAccCurr = multiCurrPrice[currency];
    if (priceAccCurr) {
      return priceAccCurr.regular_price;
    }
    return product.price;
  }


  calculateitem(items) {
    var i = 0;
    var totalPrice = 0;
    for (i = 0; i < items.length; i++) {
      totalPrice = totalPrice + this.formatPrice(items[i]) * items[i].quantity;
      console.log(items[i].price);
      console.log(items[i].quantity);
    }

    this.priceMoney = totalPrice;
  }

  alertAll(length) {
    alert('check out lenght');
    alert(length);
    alert(this.zones);
  }

  ionViewDidEnter() {

    this.language = localStorage.getItem("lan");

    if (this.language.length == 15) {
      this.language = "en";
    }

    if (this.platform.is('cordova')) {
      // this.statusBar.styleDefault();
    }

    this.setOrder();
  }

  setOrder() {

    if (this.address.getPrimary) {
      this.billing = this.address.getPrimary;
      this.shipping = this.address.getPrimary;
    }

    if (this._order.billing)
      this.billing = this._order.billing;

    if (this._order.shipping)
      this.shipping = this._order.shipping;

    //let tmp = (this.settings.value == 'shipping') ? this.shipping : this.billing;

    if (this.shipping) {

      if (this.setting.all.zones.length > 1) {
        this.zones = this.setting.all.zones;
      } else {
        this.temmZone = JSON.parse(localStorage.getItem('zone'));
        this.zones = this.temmZone;
      }

      let id = this.woo.getSingleZone(this.zones, this.shipping);
      if (id) {
        this.loader.present();
        this.woo.getShippingZoneMethod(id).then(x => {
          this.shipping_method = x;
          this.loader.dismiss();
          let tmp;
          for (let i in x) {
            if (x[i].enabled && x[i].method_id == 'free_shipping') {
              tmp = x[i].settings;
              break;
            }
          }

          if (tmp) {
            switch (tmp.requires.value) {
              case 'min_amount': // minimun order amount
                if (tmp.min_amount.value && this.cart.total > parseInt(tmp.min_amount.value))
                  this.allowFreeShipping = true;
                break;
              case 'coupon': // coupon
                if (this.coupon.description) // coupon valid
                  this.allowFreeShipping = true;
                break;
              case 'either': // minimun order amount or coupon
                if (tmp.min_amount.value && this.cart.total > parseInt(tmp.min_amount.value) || this.coupon.description)
                  this.allowFreeShipping = true;
                break;
              case 'both': // minimun order amount and coupon
                if (tmp.min_amount.value && this.cart.total > parseInt(tmp.min_amount.value) && this.coupon.description)
                  this.allowFreeShipping = true;
                break;
              default:
                this.allowFreeShipping = true;
            }
          }

        }, e => {
          console.log(e);
        });
      } else {
        this.shipping_method = [];
        this.translate.get(['NO_SHIPPING']).subscribe(x => {
          this.toast.showWithClose(x.NO_SHIPPING);
        });
      }
    } else {
      this.translate.get(['SELECT_SHIPPING']).subscribe(x => {
        this.toast.showWithClose(x.SELECT_SHIPPING);
      });
    }
  }

  viewCart() {
    this.modal.create('MiniCartPage', { isCheckout: true }, { cssClass: 'inset-modal' }).present();
  }

  setShipping(param) {
    this.shipping_lines = [];
    let e = JSON.parse(param);
    this.shipping_lines.push({
      method_id: e.method_id,
      method_title: e.title,
      total: (e.settings.cost ? e.settings.cost.value : 0).toString()
    });

    this.updateTotal();
    //this.total = this.cart.total + parseInt(this.shipping_lines[0].total);
  }

  setPayment(param) {
    this.years = [];
    let e = JSON.parse(param);
    this.order.payment_method = e.id;
    this.order.payment_method_title = e.method_title;
    this.order.payment_method_description = e.description;
    this.order.settings = e.settings;
    if (e.id != 'cod') this.order.set_paid = true;
    for (let i in this.payments)
      this.payments[i].open = false;


    for (let i in this.payments) {
      if (this.payments[i].id == e.id) {
        this.payments[i].open = true || !this.payments[i].open;
        break;
      }
    }

    if (e.id == 'stripe') {
      if (e.settings.testmode.value == 'yes') {
        this.stripe.publishable_key = e.settings.test_publishable_key.value;
        this.stripe.secret_key = e.settings.test_secret_key.value;
      } else {
        this.stripe.publishable_key = e.settings.publishable_key.value;
        this.stripe.secret_key = e.settings.secret_key.value;
      }

      for (let i = 0; i < 10; i++)
        this.years.push(new Date().getFullYear() + i);
    }

    if (e.id == 'razorpay') {
      if (e.settings.key_id.value)
        this.razor.key = e.settings.key_id.value;
      if (e.settings.key_secret.value)
        this.razor.key = e.settings.key_secret.value;
    }
  }

  listenOrder() {
    this.events.subscribe('order:go', (res) => {
      this.setOrder();
    });
  }

  selectAddress(action) {
    let params = {
      action: action
    }
    this.modal.create('SavedAddressPage', { params: params }).present();
  }

  addAddress(action) {
    let params = {
      action: action
    }
    this.modal.create('AddAddressPage', { params: params }).present();
  }

  next() {
    this.checkout = 'payment';
  }

  isCouponValid(x) {
    // console.log(this.coupon.input.toLowerCase());
    // console.log(x.toLowerCase());
    return this.coupon.input.toLowerCase() == x.toLowerCase() ? true : false;
  }

  isCouponExpired(x) {
    return new Date().getTime() > new Date(x).getTime() ? true : false;
  }

  isMinimumValid(x) {
    return this.cart.total < x ? true : false;
  }

  isMaximumValid(x) {
    return this.cart.total > x ? true : false;
  }

  submitCoupon() {
    this.allowFreeShipping = false;
    this.order.coupon_lines = [];
    this.coupon.err = '';
    this.coupon.description = '';

    if (this.coupons) {
      let errors = [
        'Coupon is invalid', 'Coupon is expired', 'Subtotal must be > ', 'Subtotal must be < ', 'Coupon usage is limited'
      ];

      let valid = false;
      let x: any;

      for (let i in this.coupons) {
        x = this.coupons[i];
        if (this.isCouponValid(x.code)) {
          valid = true;

          if (x.free_shipping)
            this.allowFreeShipping = true;

          break;
        }
      }

      if (valid) {
        if (x.date_expires && this.isCouponExpired(x.date_expires))
          this.coupon.err = errors[1];
        else if (x.minimum_amount > 0 && this.isMinimumValid(x.minimum_amount))
          this.coupon.err = errors[2] + x.minimum_amount;
        else if (x.maximum_amount > 0 && this.isMaximumValid(x.maximum_amount))
          this.coupon.err = errors[3] + x.maximum_amount;
        else if (x.usage_limit && x.usage_count >= x.usage_limit)
          this.coupon.err = errors[4];
        else {
          this.coupon.description = x.description || 'Congrat, your coupon is valid';
          this.order.coupon_lines.push({
            code: x.code,
            discount: this.getTotalDiscount(x)
          });
        }
      } else
        this.coupon.err = errors[0];

      this.updateTotal();
    }

    if (this.coupon.err) {
      this.translate.get(['OK', 'COUPON_CODE', 'COUPON_INVALID']).subscribe(x => {
        this.alert.create({
          title: x.COUPON_CODE,
          message: this.coupon.err,
          buttons: [{
            text: x.OK
          }]
        }).present();
      });
    }

  }

  getTotalDiscount(x: any) {
    let total = 0;
    if (x.discount_type == 'percent')
      total = x.amount * this.cart.total / 100;
    else if (x.discount_type == 'fixed_cart')
      total = x.amount;
    else
      total = x.amount * this.cart.totalQtyDetail;

    return total;
  }

  getTotalInDefaultCurr() {
    var i = 0;
    var totalPrice = 0;
    let items = this.cart.all;
    for (i = 0; i < items.length; i++) {
      totalPrice = totalPrice + items[i].price * items[i].quantity;
      console.log(items[i].price);
      console.log(items[i].quantity);
    }

    let shipping = this.shipping_lines[0] ? parseFloat(this.shipping_lines[0].total) : 0;
    let discount = this.coupon.description ? parseFloat(this.order.coupon_lines[0].discount) : 0;
    let total = totalPrice + shipping - discount;
    return total;
  }

  updateTotal() {
    let shipping = this.shipping_lines[0] ? parseFloat(this.shipping_lines[0].total) : 0;
    let discount = this.coupon.description ? parseFloat(this.order.coupon_lines[0].discount) : 0;

    console.log(shipping);
    console.log(discount);
    console.log(this.cart.total);

    this.total = this.priceMoney + shipping - discount;

    console.log(this.total);



    // var temp = this.total.toString();
    // var total = '';
    //
    // console.log(temp);
    //
    // if(temp.includes('.'))
    // {
    //     var temp1 = temp.substr(temp.indexOf('.'),5);
    //     var temp2 = temp.substr(0,temp.indexOf('.'));
    //
    //     total = temp2+temp1;
    //
    //     this.total = parseFloat(total);
    //
    //     console.log(this.total);
    // }else
    // {
    //
    //     console.log(this.total);
    // }
  }

  confirm() {
    this.loader.present();
    this.order.billing = this.billing;
    this.order.shipping = this.shipping;
    // this.order.line_items = this.cart.lineItems;
    this.order.line_items = this.cart.all;
    // this.order.currency = this.setting.all.settings.currency;

    if (this.coupon.description) { // update line item total if coupon valid
      let tmp: any;
      tmp = this.order.coupon_lines[0].discount / this.cart.totalQtyDetail;
      for (let i in this.order.line_items)
        this.order.line_items[i].total = this.order.line_items[i].subtotal - (tmp.toFixed(2) * this.order.line_items[i].quantity);
    }

    this.order.shipping_lines = this.shipping_lines;
    if (this.user.all) this.order.customer_id = this.user.id;

    // console.log(this.order);
    // this.loader.dismiss();
    // this.goTo('ThanksPage', this.order);
    // CHECKOUT WITH PAYPAL

    console.log(this.order);
    console.log(this.order.payment_method);

    if (this.order.payment_method == 'paypal') {
      if (!this.platform.is('cordova'))
        this.shouldDeviceOnly();
      else {
        this.order.total = this.total;
        this._order.checkoutPaypal(this.order).then((res) => {
          if (res) {
            this.createOrder(this.order);
          } else {
            this.loader.dismiss();
            this.toast.show(res);
          }
        }, err => {
          this.loader.dismiss();
          this.toast.show(err);
        });
      }

      // CHECKOUT WITH STRIPE
    } else if (this.order.payment_method == 'stripe') {
      if (!this.platform.is('cordova'))
        this.shouldDeviceOnly();
      else {
        this.order.total = this.total;
        this._order.getStripeToken(this.stripe).then((token) => {
          if (token.id) {
            token.secret_key = this.stripe.secret_key;
            this._order.checkoutStripe(this.order, token)
              .subscribe((res) => {
                this.createOrder(this.order);
              }, err => {
                this.loader.dismiss();
                this.toast.showWithClose(err.json().error.message);
              });
          } else {
            this.loader.dismiss();
            this.toast.showWithClose(token);
          }
        }, err => {
          this.loader.dismiss();
          this.toast.showWithClose(err);
        })
      }

      // CHECKOUT WITH RAZOR PAY
    } else if (this.order.payment_method == 'razorpay') {
      if (!this.platform.is('cordova'))
        this.shouldDeviceOnly();
      else {
        this.order.total = this.total;
        this._order.checkoutRazorpay(this.order, this.razor)
          .then((res) => {
            console.log(res);
            this.createOrder(this.order);
          }, err => {
            this.loader.dismiss();
            this.toast.showWithClose(err.description);
          });
      }

      // CHECKOUT WITH OTHERs
    } else if (this.order.payment_method == 'bacs' || this.order.payment_method == 'cod' || this.order.payment_method == 'cheque') {
      this.order.line_items = this.cart.lineItems;
      this.order.currency = "KWD";
      // this.order.total = this.getTotalInDefaultCurr();
              this.order.total = this.total;
      this.createOrder(this.order);

      // NOT AVAILABLE
    } else if (this.order.payment_method == 'hesabe') {
      console.log("Hesabe");

      if (!this.platform.is('cordova')) {
        this.shouldDeviceOnly();
      } else {
        this.order.currency = "KWD";
        // this.order.total = this.getTotalInDefaultCurr();
        this.order.total = this.total;
        this.hesabe.payRequest(this.order).then((res: any) => {
          this.openInappBrowser(res.paymentUrl, res.successUrl, res.errorUrl, (status) => {

            if (status) {
              this.order.line_items = this.cart.lineItems;
              this.createOrder(this.order);
            } else {
              this.toast.showWithClose("Error")
            }

          });
        }, (error) => {

          console.log("My Fatoorah Error");

        });
      }
    } else {
      this.loader.dismiss();
      this.translate.get(['OK', 'NOT_AVAILABLE', 'PAYMENT_NOT_AVAILABLE']).subscribe(x => {
        this.alert.create({
          title: x.NOT_AVAILABLE,
          message: x.PAYMENT_NOT_AVAILABLE,
          buttons: [{
            text: x.OK
          }]
        }).present();
        this.loader.dismiss();
        return false;
      });
    }
  }


  openInappBrowser(url, succesUrl, errorUrl, callBack) {
    const browser = this.iab.create(url, '_blank');
    browser.on("loadstart").subscribe(event => {
      if (event.url.indexOf(succesUrl) != -1) {
        callBack(true);
        browser.close();
        console.log("InApp Browser Success Url");
      } else if (event.url.indexOf(errorUrl) != -1) {
        callBack(false);
        browser.close();
        console.log("InApp Browser Error Url");
      }
    });

    browser.on("exit").subscribe(() => {

      this.loader.dismiss();

    }, error => { });
  }


  shouldDeviceOnly() {
    this.translate.get(['OK', 'ONLY_DEVICE', 'ONLY_DEVICE_DESC']).subscribe(x => {
      this.alert.create({
        title: x.ONLY_DEVICE,
        message: x.ONLY_DEVICE_DESC,
        buttons: [{
          text: x.OK
        }]
      }).present();
      this.loader.dismiss();
      return false;
    });
  }

  createOrder(order: any) {
    console.log("order");
    console.log(order);
    this.order.status = "processing";
    this.woo.createOrder(order).then(x => {
      if (x) {
        console.log("X");
        console.log(x);
        this._order.reset().then(() => { });
        this.cart.reset().then(() => { });
        this.goTo('ThanksPage', x);
      } else {
        this.toast.showWithClose('CORS issues, please use https.');
      }
      this.loader.dismiss();
    }, err => {
      this.loader.dismiss();
      this.toast.show(err);
    })
  }

  goTo(page, params) {
    this.app.getRootNav().setRoot(page, { params: params });
  }

  myfaroorah() {

  }

}
