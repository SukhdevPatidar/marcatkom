import {Component} from '@angular/core';
import {StatusBar} from '@ionic-native/status-bar';
import {Keyboard} from '@ionic-native/keyboard';
import {IonicPage, Platform, NavController, ModalController, Events} from 'ionic-angular';
import {
    WooCommerceProvider,
    ToastProvider,
    LoadingProvider,
    WishlistProvider,
    CartProvider,
    SettingsProvider
} from '../../providers/providers';
import {TranslateService} from '@ngx-translate/core';
import {App} from '../../app/app.global';
import {ProductPage} from "../product/product";

@IonicPage()
@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})

export class HomePage {
    categories: any[] = [];
    data: any[] = [];
    app: any;

    constructor(private cart: CartProvider, public events: Events, public nav: NavController, private platform: Platform, private settings: SettingsProvider, private keyboard: Keyboard, private statusBar: StatusBar, private translate: TranslateService, private toast: ToastProvider, public wishlist: WishlistProvider, public loader: LoadingProvider, public modalCtrl: ModalController, private woo: WooCommerceProvider) {
        this.loader.present();

        this.woo.getAllCategories(10).then((tmp) => { // get all categories
            this.categories = tmp;
            let root = [];
            this.woo.loadSetting().then(x => {
                this.app = x;
                if (x.currency && tmp && tmp.length > 0) {
                    var count = 0;
                    for (let i in tmp) {
                        if (tmp[i].count > 0) {
                            count++;
                            let item = {
                                data: tmp[i],
                                items: []
                            };
                            this.woo.getAllProducts(null, tmp[i].id, null, null, null, 9, null, 'asc', 'title').then((val) => {
                                if (val && val.length > 0) {
                                    let product = val[0];
                                    let productCurr = product['multi-currency-prices'];
                                    var allCurrency = [];
                                    allCurrency.push({id: "KWD", title: "KWD"});
                                    Object.keys(productCurr).map((currency) => {
                                        allCurrency.push({id: currency, title: currency});
                                    });
                                    App.currencies = allCurrency;
                                }
                                item.items = val;
                                root.push(item);
                                if (root.length >= count) {
                                    setTimeout(() => {

                                        this.loader.dismiss();

                                    }, 1000);
                                }
                            })
                        }
                    }
                    if (count == 0) {
                        this.loader.dismiss();
                    }
                } else {
                    this.loader.dismiss();
                }
            }, () => {
                this.loader.dismiss();
            });
            this.data = root;
        }, err => {
            this.loader.dismiss();
            this.toast.show(err);
        })
    }


    ionViewDidLoad() {
        this.events.subscribe("TABCHANGE", (index) => {
            console.log(index);
            if (index == 0) {
                this.settings.load().then((x) => {
                    if (x) {
                        this.app = x.settings;
                    }
                });
            }
        });
    }

    ionViewWillEnter() {
        if (this.platform.is('cordova')) {
            // this.statusBar.styleLightContent();
            this.keyboard.disableScroll(false);
            this.keyboard.hideKeyboardAccessoryBar(false);
        }
    }

    formatPrice = (product) => {
        let multiCurrPrice = product['multi-currency-prices'];
        let currency = this.settings.all.settings ? this.settings.all.settings.currency : this.settings.all.currency;
        let priceAccCurr = multiCurrPrice[currency];
        if (priceAccCurr) {
            return priceAccCurr.regular_price;
        }
        return product.price;
    }

    viewCart() {
        this.modalCtrl.create('MiniCartPage', {}, {cssClass: 'inset-modal'}).present();
    }

    addToCart(product: any) {
        if (product.type == 'variable') {
            this.goTo('ProductPage', product);
        } else {
            this.translate.get(['ADDED_TO_CART']).subscribe(x => {
                this.cart.post(product);
                this.toast.show(x.ADDED_TO_CART);
            });
            this.viewCart();
        }
    }

    setFav(product: any) {
        this.translate.get(['REMOVE_WISH', 'ADDED_WISH']).subscribe(x => {
            let msg = product.isFav ? x.REMOVE_WISH : x.ADDED_WISH;
            this.wishlist.post(product);
            product.isFav = product.isFav ? false : true;
            this.toast.show(msg);
        });
    }

    showSearch() {
        this.modalCtrl.create('SearchPage').present();
    }

    goTo(page, params) {

        console.log(page);

        if (params) {
            this.nav.push(page, {params: params});
        }
    }

}
