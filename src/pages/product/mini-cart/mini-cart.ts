import { Component } from '@angular/core';
import { ViewController, ModalController, AlertController, App, IonicPage, NavController, NavParams } from 'ionic-angular';
import { CartProvider, ToastProvider, LoadingProvider, UserProvider ,SettingsProvider} from '../../../providers/providers';
import { TranslateService } from '@ngx-translate/core';

@IonicPage()
@Component({
  selector: 'page-mini-cart',
  templateUrl: 'mini-cart.html',
})
export class MiniCartPage {
  shouldAnimate: boolean = true;
  total: number = 0;
  isCheckout: boolean = false;

  language:any;

  priceMoney:any = 0;

  constructor(public nav: NavController,private translate: TranslateService, public setting: SettingsProvider,private toast: ToastProvider, private modal: ModalController, private alert: AlertController, private user: UserProvider, private cart: CartProvider, private appCtrl: App, public loader: LoadingProvider, public viewCtrl: ViewController, public navCtrl: NavController, public navParams: NavParams) {
    this.isCheckout = this.navParams.data.isCheckout;
    let product = this.navParams.data.product;
    if(this.navParams.data.product)
      this.cart.post(product, 1);

    console.log(this.cart);
    this.calculateitem(this.cart.all);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  formatPrice = (product) => {
		let multiCurrPrice = product['multi-currency-prices'];
		let currency = this.setting.all.settings?this.setting.all.settings.currency:this.setting.all.currency;
		let priceAccCurr = multiCurrPrice[currency];
		if(priceAccCurr) {
			return priceAccCurr.regular_price;
		}
		return product.price;
  }
  
  ionViewDidEnter()
  {
      console.log("MiniCart");

      this.language = localStorage.getItem("lan");

      if(this.language.length == 15)
      {
          this.language = "en";
      }

      console.log(this.cart.total);
  }

    goHome(){
        this.viewCtrl.dismiss();
    }

  remove(product) {
    this.translate.get(['REMOVE_TTL', 'REMOVE_MSG', 'CANCEL', 'YES']).subscribe( x=> {
      this.alert.create({
        title: x.REMOVE_TTL,
        message: x.REMOVE_MSG,
        buttons: [{
            text: x.CANCEL
          },{
            text: x.YES,
            handler: () => {
              this.confirmRemove(product);
            }
          }]
      }).present();
    });
  }

  confirmRemove(product: any){
    this.cart.remove(product);
    this.translate.get('REMOVE_FROM_CART').subscribe( x=> {
      this.toast.show(x);
    });
    console.log("remove");
    this.calculateitem(this.cart.all);
  }

  goCheckout(){
    if(this.user.all){
      this.dismiss();
      this.appCtrl.getRootNav().push('CheckoutPage');
    }
    else{
      this.translate.get(['CHECKOUT_GUEST', 'CHECKOUT_GUEST_MSG', 'NO', 'YES']).subscribe( x=> {
        this.alert.create({
          title: x.CHECKOUT_GUEST,
          message: x.CHECKOUT_GUEST_MSG,
          buttons: [{
              text: x.NO,
              handler: () => {
                this.dismiss();
                this.appCtrl.getRootNav().push('CheckoutPage');
              }
            },{
              text: x.YES,
              handler: () => {
                this.dismiss();
                this.modal.create('LoginPage').present();
              }
            }]
        }).present();
      });
    }
  }

  calculateitem(items)
  {
    var i = 0;
    var totalPrice = 0;
      for(i=0;i < items.length; i++)
      {
        totalPrice = totalPrice + this.formatPrice(items[i]) * items[i].quantity;
        console.log(items[i].price);
        console.log(items[i].quantity);
      }

      this.priceMoney = totalPrice;
  }

  additem(x,y)
  {
      this.cart.post(x,y);
      this.calculateitem(this.cart.all);
  }
  minusitem(x,y)
  {
      this.cart.post(x,y);
      this.calculateitem(this.cart.all);
  }
}
