import { VariationModal } from './variation-modal';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SharedModule } from '../../../app/shared.module';

@NgModule({
  declarations: [
    VariationModal
  ],
  imports: [
    IonicPageModule.forChild(VariationModal),
    SharedModule
  ],
  exports: [
    VariationModal
  ]
})

export class VariationModalModule { }
