import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import * as xml2js from 'xml2js';

/*
  Generated class for the HesabepaymentProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class HesabepaymentProvider {

  constructor(public http: Http) {
    console.log('Hello HesabepaymentProvider Provider');
  }


  payRequest(order)
  {
    var ref = this;
    var settings = order.settings;
    var url = 'https://www.hesabe.com/authpost';
    // var url = 'http://localhost:8080/soapUrl';
    var orderCurrency = order.currency;
    var orderTotal = parseFloat(order.total).toFixed(3);
    var products = '';
    var items = order.line_items;
    var mechantcode = '8841218';
    var successUrl = 'http://playcontest.in/payment/hesabe/success.php';
    var errorUrl = 'http://playcontest.in/payment/hesabe/error.php';
    var Variable1 = order.payment_method_description;
    var Variable2 = order.billing.first_name;
    var Variable3 = order.billing.email;
    var Variable4 = order.billing.phone;
    var Variable5 = order.customer_id;

    var paymentUrl = '';


    var data = {MerchantCode:mechantcode,Amount:orderTotal,Currency:'USD', SuccessUrl:successUrl,FailureUrl:errorUrl,Variable1:Variable1,Variable2:Variable2,Variable3:Variable3,Variable4:Variable4,Variable5:Variable5};


    console.log(data);

    return new Promise((resolve,reject)=>{

      let headers = new Headers();
      let options = new RequestOptions({headers:headers});

      this.http.post(url,data,options).map((res)=>{
        return res.json();

      }).subscribe(data=>{

          console.log(data);

          if(data.status == "success")
          {
              paymentUrl = data.data.paymenturl + data.data.token;

              resolve({

                  paymentUrl:paymentUrl,
                  successUrl:successUrl,
                  errorUrl:errorUrl

              });
          }else
          {
              resolve({

                  paymentUrl:paymentUrl,
                  successUrl:successUrl,
                  errorUrl:errorUrl

              });
          }


      },(error)=>{

        console.log("Error"+JSON.stringify(error));
        reject(error);

      });

    });


  }

}
