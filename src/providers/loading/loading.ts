import { Injectable } from '@angular/core';
import { LoadingController } from 'ionic-angular';

@Injectable()
export class LoadingProvider {
  load: any;

  constructor(private loader: LoadingController) {}

  present() {
    this.load = this.loader.create({
      spinner: 'dots'
    });
    
    this.load.present(); 
  }

  dismiss(){
    this.load.dismiss();
  }
}
