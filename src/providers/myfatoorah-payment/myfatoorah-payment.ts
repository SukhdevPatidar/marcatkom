import { Injectable } from '@angular/core';
import { Http, RequestOptions, Headers } from '@angular/http';
import * as xml2js from 'xml2js';

/*
  Generated class for the MyfatoorahPaymentProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
  */
  @Injectable()
  export class MyfatoorahPaymentProvider {

  	constructor(public http: Http) {
  		console.log('Hello MyfatoorahPaymentProvider Provider');
  	}


  	soapRequest(order)
    {
      var ref = this;
      var settings = order.settings;
      var url = settings.api_url.value;
      var userName = settings.api_username.value;
      var password = settings.api_password.value;
      var paymentMode = settings.payment_mode.value;
      var vendorCode = settings.vendor_code.value;
      var successUrl = url+"/success";
      var errorUrl = url+"/error";
      var customerName = order.billing.first_name;
      var customerEmail = order.billing.email;
      var customerMobile = order.billing.phone;

      var orderCurrency = order.currency;
      var orderTotal = order.total * 100;

      var products  = '';
      var items = order.line_items;


      // test credentials

      // url = "https://test.myfatoorah.com/pg/PayGatewayServiceV2.asmx";
      // vendorCode = "999999";
      // userName = "testapi@myfatoorah.com";
      // password = "E55D0";
      // successUrl = "https://test.myfatoorah.com/pg/PayGatewayServiceV2.asmx/success";
      // errorUrl = "https://test.myfatoorah.com/pg/PayGatewayServiceV2.asmx/error";


      for (var i = 0; i < items.length; ++i) {
        var item = items[i];
        products = products + '<ProductDC>';
        products = products + '<product_name>'+item.product_id+'</product_name>';
        products = products + '<unitPrice>'+parseFloat(item.subtotal)+'</unitPrice>';
        products = products + '<qty>'+item.qty+'</qty>';
        products = products + '</ProductDC>';
      }


      var requestData = '<?xml version="1.0" encoding="windows-1256"?>'+
      '<soap12:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap12="http://www.w3.org/2003/05/soap-envelope">'+
      '<soap12:Body><PaymentRequest xmlns="http://tempuri.org/"><req><CustomerDC>'+
      '<Name>'+customerName+'</Name>'+
      '<Email>'+customerEmail+'</Email>'+
      '<Mobile>'+customerMobile+'</Mobile>'+
      '</CustomerDC>'+
      '<MerchantDC>'+
      '<merchant_code>'+vendorCode+'</merchant_code>'+
      '<merchant_username>'+userName+'</merchant_username>'+
      '<merchant_password>'+password+'</merchant_password>'+
      '<merchant_ReferenceID>201454542102</merchant_ReferenceID>'+
      '<ReturnURL>'+successUrl+'</ReturnURL>'+
      '<merchant_error_url>'+errorUrl+'</merchant_error_url>'+
      '</MerchantDC>'+
      '<lstProductDC>'+
      products+
      '</lstProductDC>'+
      '<totalDC>'+
      '<subtotal>'+orderTotal+'</subtotal>'+
      '</totalDC>'+
      '<paymentModeDC>'+
      '<paymentMode>'+paymentMode+'</paymentMode>'+
      '</paymentModeDC>'+
      '<paymentCurrencyDC>'+
      '<paymentCurrrency>'+orderCurrency+'</paymentCurrrency>'+
      '</paymentCurrencyDC>'+
      '</req>'+
      '</PaymentRequest>'+
      '</soap12:Body>'+
      '</soap12:Envelope>';



      return new Promise((resolve, reject) => {

        let headers = new Headers({ 'Content-Type': 'text/xml' });
        let options = new RequestOptions({ headers: headers });

        this.http.post(url, requestData ,options)
  			// .timeout(20000, new Error('Request Timeout'))
  			.map((res) => {
  				return res.text();
  			})
  			.subscribe(data => {

  				var resData;
  				xml2js.parseString(data, (error, result) => {
  					resData = result;
  				});

  				let soapEnevelop = resData['soap:Envelope']['soap:Body'];
  				if(soapEnevelop && soapEnevelop.length > 0) {
  					let paymentRequestResponse = soapEnevelop[0]['PaymentRequestResponse'];
  					if(paymentRequestResponse && paymentRequestResponse.length > 0) {
  						let paymentRequestResult = paymentRequestResponse[0]['PaymentRequestResult'];
  						if(paymentRequestResult && paymentRequestResult.length > 0) {
  							let paymentResponse = paymentRequestResult[0];

  							if(paymentResponse['ResponseCode'] == 0 && paymentResponse['ResponseMessage'] == "Success") {
  								console.log("paymentURL " + paymentResponse.paymentURL);
                  resolve({
                    paymentUrl:paymentResponse.paymentURL,
                    successUrl: successUrl,
                    errorUrl:errorUrl
                  });
                } else {
                  // resolve(paymentResponse);
                }
              }
            }
          }
        },
        (error) => {
          console.log("Error "+JSON.stringify(error));
          reject(error);
        }
        );
  		});
    }

  }
