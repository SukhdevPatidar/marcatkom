import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';

@Injectable()
export class SettingsProvider {
  public SETTINGS_KEY: string = 'settings';

  public settings: any = {};
  public _readyPromise: Promise<any>;

  public tempzone:any = [];

  constructor(public storage: Storage) {
    this.load();
  }

  load() {

    var zonesize = 0;
    return this.storage.get(this.SETTINGS_KEY).then((val) => {
      if (val) {

        if(val.zones)
        {
            zonesize = val.zones.length;

            if(zonesize > 1)
            {
              localStorage.setItem("zone",JSON.stringify(val.zones));
            }

        }
        this.settings = val;
        return this.settings;
      } else {
        this.storage.set(this.SETTINGS_KEY, this.settings);
      }
    });
  }

  public getCountries(){
    let countries = [];
    let val = this.settings.countries;
    if(val.value.length == 0){
      for(let i in val.options)
        countries.push({id: i, name: val.options[i]});
    }else{
      for(let i in val.value){
        countries.push({id: val.value[i], name: val.options[val.value[i]]});
      }
    }
    return countries;
  }

  public setSettings(data, id: string){
    let tmp = this.settings;
    tmp[id] = data;
    localStorage.setItem("lan",data);
    return this.save(tmp);
  }

  public save(data){
    this.settings = data;
    return this.storage.set(this.SETTINGS_KEY, data);
  }

  public get all() {
    return this.settings;
  }
}
